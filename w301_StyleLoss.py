from tensorflow.keras import losses as ls
from tensorflow.keras import backend as K
from tensorflow.keras.applications import vgg19
from tensorflow.keras.layers import Input
from tensorflow.keras.models import Model

img_ch = 3

time = '2020-0412-1133'
print('current style loss version : '+time)

content_layers = [
    # 'block1_conv1',
    # 'block2_conv1',
    # 'block3_conv1',
    # 'block4_conv1',
    'block5_conv1'
]

style_layers = [
    # 'block1_conv2',
    # 'block2_conv2',
    # 'block3_conv2',
    # 'block4_conv2',
    'block5_conv4'
]

content_layer = 'block2_conv2'
model_content = vgg19.VGG19(input_shape=(None, None, img_ch), weights='imagenet', include_top=False)
model_content.trainable = False
for l in model_content.layers:
    l.trainable = False
model_content = Model(inputs=model_content.input, outputs=model_content.get_layer(content_layer).output)
model_content.trainable = False

style_layer = 'block5_conv4'
model_style = vgg19.VGG19(input_shape=(None, None, img_ch), weights='imagenet', include_top=False)
model_style.trainable = False
for l in model_style.layers:
    l.trainable = False
model_style = Model(inputs=model_style.input, outputs=model_style.get_layer(style_layer).output)
model_style.trainable = False


def gram_matrix(x):
    features = K.batch_flatten(K.permute_dimensions(x, (3, 0, 1, 2)))
    gram = K.dot(features, K.transpose(features))
    return gram


def content_loss(y_true, y_pred):
    x1 = model_content(y_pred)
    x0 = model_content(y_true)
    return K.sum(K.square(x1 - x0)) / K.prod(K.cast(K.shape(y_true), 'float32'))


def style_loss(y_true, y_pred):
    x1 = model_style(y_pred)
    x0 = model_style(y_true)
    X1 = gram_matrix(x1)
    X0 = gram_matrix(x0)
    return K.sum(K.square(X1 - X0)) / K.prod(K.cast(K.shape(y_true), 'float32'))


def total_variation_loss(x):
    a = K.square(x[:, :- 1, :- 1, :] - x[:, 1:, :- 1, :])
    b = K.square(x[:, :- 1, :- 1, :] - x[:, :- 1, 1:, :])
    return K.sum(K.pow(a + b, 1.25))
