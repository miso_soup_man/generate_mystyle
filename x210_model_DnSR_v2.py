import x110_load_DnSR_v2 as ld
from keras.layers import Conv2D, BatchNormalization, Activation, Input, Concatenate, MaxPool2D, Conv2DTranspose, Add, Masking
from keras.models import Model
from keras.optimizers import Adam
from keras.objectives import mean_squared_error
from keras import backend as K
from keras import callbacks as cb
from keras import losses
from os import path
import os
import numpy as np
import cv2
import matplotlib.pyplot as plt
from datetime import datetime as dt
from tqdm import tqdm

time = '2020-0311-2355'
print('current version : '+time)


class CNN:
    # construction
    def __init__(self):
        # initialize
        self.description = 'default configuration ('+time+')'
        self.data_val = []
        self.data_pred = []
        self.epochs = 300
        self.patch_size = 64
        self.stride = 24
        self.augment = 1
        self.callbacks = []
        self.model_path = ''
        self.modeltype = 'resnet_v1'
        self.model = Model()
        self.save_distance = 10
        self.ext = 'png'
        self.steps_per_epoch = 200
        self.batch_size = 64
        self.temp_width = 1

        # construction
        self.data_train = []
        self.init_model()
        self.init_callbacks()
        self.optimizer = Adam(beta_1=0.9, beta_2=0.999, amsgrad=False)
        self.model.summary()

    def reset(self):
        self.__init__()

    # # preparation
    def load_weights(self, path):
        self.model.load_weights(path)
        self.model.compile(optimizer=self.optimizer, loss='mse')

    def load_train_data(self, path):
        self.data_train = ld.load_datapatches(
            path, self.patch_size, self.stride, self.ext, self.augment,
            temp_width=self.temp_width
        )

    def init_model(self):
        if self.modeltype == 'unet_v1':
            self.model = self.unet_v1()
        elif self.modeltype == 'simple_cnn_v1':
            self.model = self.simple_cnn_v1()
        elif self.modeltype == 'skipnet_v1':
            self.model = self.skipnet_v1()
        elif self.modeltype == 'resnet_v1':
            self.model = self.resnet_v1()

    def unet_v1(self):
        def __model_block(input_layer, chs):
            x = input_layer
            for i in range(2):
                x = Conv2D(chs, 3, padding='same')(x)
                x = BatchNormalization()(x)
                x = Activation('relu')(x)
            return x

        self.modeltype = 'unet_v1'
        model_input = Input((None, None, 1+self.temp_width*2))

        # encoder
        block1 = __model_block(model_input, 16)
        x = MaxPool2D(2)(block1)
        block2 = __model_block(x, 32)
        x = MaxPool2D(2)(block2)
        block3 = __model_block(x, 64)
        x = MaxPool2D(2)(block3)
        block4 = __model_block(x, 128)
        # x = MaxPool3D(2)(block4)

        # bottom
        x = MaxPool2D(2)(block4)
        x = __model_block(x, 256)

        #  decoder
        x = Conv2DTranspose(128, kernel_size=2, strides=2)(x)
        x = __model_block(x, 128)

        x = Conv2DTranspose(64, kernel_size=2, strides=2)(x)
        x = __model_block(x, 64)

        x = Conv2DTranspose(32, kernel_size=2, strides=2)(x)
        x = __model_block(x, 32)

        x = Conv2DTranspose(16, kernel_size=2, strides=2)(x)
        x = __model_block(x, 16)

        x = Conv2DTranspose(8, kernel_size=2, strides=2)(x)
        x = __model_block(x, 8)

        x = Conv2D(1, kernel_size=[1, 1])(x)
        x = Activation('sigmoid')(x)

        return Model(model_input, x)

    def resnet_v1(self):
        def __model_block(input_layer, chs):
            x = input_layer
            for i in range(2):
                x = Conv2D(chs, kernel_size=(3, 3), padding='same', strides=(1, 1))(x)
                x = BatchNormalization()(x)
                x = Activation('relu')(x)
            return x

        self.modeltype = 'unet_v1'
        model_input = Input((None, None, 1+self.temp_width*2))

        # encoder
        block1 = __model_block(model_input, 16)
        x = MaxPool2D(2)(block1)
        block2 = __model_block(x, 32)
        x = MaxPool2D(2)(block2)
        block3 = __model_block(x, 64)
        x = MaxPool2D(2)(block3)
        block4 = __model_block(x, 128)
        # x = MaxPool3D(2)(block4)

        # bottom
        x = MaxPool2D(2)(block4)
        x = __model_block(x, 256)

        #  decoder
        x = Conv2DTranspose(128, kernel_size=2, strides=2)(x)
        x = __model_block(x, 128)

        x = Conv2DTranspose(64, kernel_size=2, strides=2)(x)
        x = __model_block(x, 64)

        x = Conv2DTranspose(32, kernel_size=2, strides=2)(x)
        x = __model_block(x, 32)

        x = Conv2DTranspose(16, kernel_size=2, strides=2)(x)
        x = __model_block(x, 16)

        x = Conv2DTranspose(8, kernel_size=2, strides=2)(x)
        x = __model_block(x, 8)

        x = Conv2D(1, kernel_size=[1, 1])(x)
        x = Activation('sigmoid')(x)

        return Model(model_input, x)

    def skipnet_v1(self):
        def __model_block(input_layer, chs):
            x = input_layer
            for i in range(2):
                x = Conv2D(chs, 3, padding='same')(x)
                x = BatchNormalization()(x)
                x = Activation('relu')(x)
            return x

        self.modeltype = 'skipnet_v1'
        model_input = Input((None, None, None))

        # encoder
        block1 = __model_block(model_input, 16)
        x = MaxPool2D(2)(block1)
        block2 = __model_block(x, 32)
        x = MaxPool2D(2)(block2)
        block3 = __model_block(x, 64)
        x = MaxPool2D(2)(block3)
        block4 = __model_block(x, 128)
        x = MaxPool2D(2)(block4)

        # bottom
        x = MaxPool2D(2)(block4)
        x = __model_block(x, 256)

        #  decoder
        x = Conv2DTranspose(128, kernel_size=2, strides=2)(x)
        x = __model_block(x, 128)

        x = Conv2DTranspose(64, kernel_size=2, strides=2)(x)
        x = __model_block(x, 64)

        x = Conv2DTranspose(32, kernel_size=2, strides=2)(x)
        x = __model_block(x, 32)

        x = Conv2DTranspose(16, kernel_size=2, strides=2)(x)
        x = __model_block(x, 16)

        x = Conv2DTranspose(8, kernel_size=2, strides=2)(x)
        x = __model_block(x, 16)

        x = Conv2D(1, kernel_size=[1, 1])(x)
        x = Activation('sigmoid')(x)

        return Model(model_input, x)

    def simple_cnn_v1(self):
        def __model_block(input_layer, chs):
            x = input_layer
            for i in range(2):
                x = Conv2D(chs, 3, padding='same')(x)
                x = BatchNormalization()(x)
                x = Activation('relu')(x)
            return x

        self.modeltype = 'simple_cnn_v1'
        model_input = Input((None, None, None))

        # encoder
        x = __model_block(model_input, 16)
        x = __model_block(x, 32)
        x = __model_block(x, 64)
        # x = __model_block(x, 128)
        # x = __model_block(x, 256)
        x = __model_block(x, 128)
        x = __model_block(x, 64)
        x = __model_block(x, 32)
        x = __model_block(x, 16)

        x = Conv2D(1, kernel_size=[1, 1])(x)
        x = Activation('sigmoid')(x)

        return Model(model_input, x)

    def loss_function(self, y_true, y_pred):
        mses = mean_squared_error(y_true, y_pred)
        return K.sum(mses, axis=(1, 2, 3))

    def compile_train(self):
        self.model.compile(optimizer=self.optimizer, loss='mse')

        self.model.fit(
            x=self.data_train[0], y=self.data_train[1], batch_size=self.batch_size,
            verbose=1, epochs=self.epochs, callbacks=self.callbacks
        )

    def init_callbacks(self):
        ret = []
        fpath = path.join(self.model_path, self.modeltype + '_weights.{epoch:0=5}-{loss:.5f}.hdf5')
        tmp = cb.ModelCheckpoint(
            filepath=fpath, verbose=1, save_weights_only=False, period=self.save_distance
        )
        ret.append(tmp)
        self.callbacks = ret

    # # evaluation
    def load_val_data(self, path):
        self.data_val = ld.load_val_data(path)

    def evaluate(self, verbose=True):
        x = self.data_val[0]
        img_num = x.__len__()
        ret = 0
        for idx in range(x.__len__()):
            curr_y1 = self.model.predict(x=x[idx])
            curr_y0 = self.data_val[1][idx]
            ret += (curr_y1**2 - curr_y0**2) ** 0.5

        ret = ret/img_num
        if verbose:
            print('mean square error: {:.5f}'.format(ret))

        return ret

    # # prediction
    def load_predict_data(self, tgt_dir: str):
        self.data_pred = ld.load_predict_data(tgt_dir)

    def predict(self, save_img: bool = False, save_path: str = ''):
        if save_img:
            save_path = path.join(save_path, timestr(3)+'_predicted/')
            if not(path.exists(save_path)):
                os.mkdir(save_path)

        src_list = self.data_pred
        dst_list = []
        count = 0

        for src_img in tqdm(src_list):
            dst_img = []
            for ch in range(src_img.shape[3]):
                curr_ch = src_img[:, :, :, ch][:, :, :, np.newaxis]
                curr_ch = np.transpose(curr_ch, [3, 0, 1, 2])
                dst_img.append(np.squeeze(self.model.predict(x=curr_ch)))
            dst_img = np.transpose(np.array(dst_img), [1, 2, 0])
            dst_list.append(dst_img)

            if save_img:
                save_path_img = path.join(save_path, '{:05d}.png'.format(count))
                dst_img = np.squeeze(dst_img*65535)
                dst_img = dst_img.astype(dtype=np.uint16)
                cv2.imwrite(save_path_img, dst_img)

            count += 1

        return dst_list

    def view_train(self, fignum: int = 100, samples: int=100):
        data_in = self.data_train[0]

        for idx in range(data_in.shape[0]):
            if (samples < idx) & samples:
                break

            tmp_src0 = np.squeeze(data_in[idx, :, :, 0, :])
            view(tmp_src0, fignum=fignum)

    def view_pred(self, fignum: int = 100):
        data_in = self.data_pred

        for img in data_in:
            for fr in range(img.shape[2]):
                tmp = img[:, :, fr, :]
                view(tmp, fignum=fignum)


def view(data: np.ndarray, fignum: int = 100):
    data = data.copy()[:, :, ::-1]
    plt.clf()
    fig, ax = plt.subplots(num=fignum)
    ax.imshow(np.squeeze(data))
    plt.show()
    plt.pause(0.5)


def timestr(mode=0):
    tmp = dt.now()
    ret = 'xxxx-xxxx_'

    if mode == 0:
        print('*** help docs ****')
        print('1: 2019-0329')
        print('2: 2019-0329_')
        print('3: 2019-0329-1201')

    elif mode == 1:
        ret = "{0:%Y-%m%d}".format(tmp)

    elif mode == 2:
        ret = "{0:%Y-%m%d_}".format(tmp)

    elif mode == 3:
        ret = "{0:%Y-%m%d-%H%M}".format(tmp)

    return ret


