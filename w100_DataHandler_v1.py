import cv2
import numpy as np
from glob import glob
from itertools import product
import os
from tqdm import tqdm, trange
import random
import matplotlib.pyplot as plt
from matplotlib import animation
import math

time = '2020-0406-1944'
print('current handler version : '+time)


class DataSet:
    def __init__(
            self,
            path: str,
            ext: str = 'png',
            data_mode='train',
            reduce_factr=1.,
            patch_size=64,
            stride=10,
            augment=1,
            channel_mode='single',
            patch_limit:int = 30000,
            down_convert_fctr:int = 2,
            resize: np.ndarray = np.array([0, 0])
            ):

        self.path = path
        self.x = np.array([])
        self.y = np.array([])
        self.y_pred = np.array([])
        self.data_mode = data_mode
        self.channel_mode = channel_mode # 'multi', 'single'
        self.ext = ext
        self.original_size = np.ndarray([0, 0])
        self.resize = resize

        # option for patch splitting
        self.patch_size = patch_size
        self.stride = stride
        self.augment = augment
        self.reduce_factr = reduce_factr
        self.seed = None
        self.down_convert_fctr = down_convert_fctr
        self.patch_limit = patch_limit

        self.load()

    def load(self):
        path = self.path
        print('data will be loaded from ' + path)
        print('loading images...')

        img_buff_src = []
        img_buff_dst = []
        
        if (self.data_mode == self.DataMode_TRAIN()) or (self.data_mode == self.DataMode_VAL()):
            if self.channel_mode == self.CH_SINGLE():
                path = self.__list_path_single_ch(path, self.ext)

                for idx in trange(path[0].__len__()):
                    path_src = path[0][idx]
                    path_dst = path[1][idx]
                    img_buff_src.append(self.__load_img(path_src))
                    img_buff_dst.append(self.__load_img(path_dst))

            elif self.channel_mode == self.CH_MULTI(): 
                path = self.__list_path_multi_ch(path, self.ext)
                for idx in trange(path[0].__len__()):
                    path_src0 = path[0][idx]
                    path_src1 = path[1][idx]
                    img0 = self.__load_img(path_src0)
                    img1 = self.__load_img(path_src1)
                    img_buff_src.append(np.concatenate((img0, img1), axis=2))
                    path_dst = path[2][idx]
                    img_buff_dst.append(self.__load_img(path_dst))

        else:  # case: product
            if self.channel_mode == self.CH_SINGLE():
                path = self.__list_path_single_ch_nodst(path, self.ext)
                for idx in trange(path.__len__()):
                    path_src = path[idx]
                    img_buff_src.append(self.__load_img(path_src))

            elif self.channel_mode == self.CH_MULTI():
                path = self.__list_path_multi_ch_nodst(path, self.ext)
                for idx in trange(path[0].__len__()):
                    path_src0 = path[0][idx]
                    path_src1 = path[1][idx]
                    img0 = self.__load_img(path_src0)
                    img1 = self.__load_img(path_src1)
                    img_buff_src.append(np.concatenate((img0, img1), axis=2))

        if self.data_mode == self.DataMode_TRAIN():  # patch generating section
            patches_src = []
            patches_dst = []
            print('generating patches')
            for idx in tqdm(range(img_buff_src.__len__())):
                patches_src = patches_src + self.gen_patches(img_buff_src[idx])
                patches_dst = patches_dst + self.gen_patches(img_buff_dst[idx])

                if len(patches_src) > self.patch_limit:
                    print('Number of patches has reached to the limit. {} patches will be returned.'.format(len(patches_src)))
                    break

            patches_src, patches_dst = self.__shuffle(patches_src, patches_dst, seed=self.seed)
            
            n = round(len(patches_src)*self.reduce_factr)
            patches_src = patches_src[0:n]
            patches_dst = patches_dst[0:n]

            patches_dst, patches_src = self.__dropout(patches_dst, patches_src)
            if self.down_convert_fctr > 1:
                patches_src = self.__dwnconvert(patches_src, fctr=self.down_convert_fctr)
            
            if self.augment > 1:
                patches_dst, patches_src = self.__augment(patches_dst, patches_src, fctr=self.augment)

            if self.down_convert_fctr > 1:
                patches_src = self.__dwnconvert(patches_src, fctr=self.down_convert_fctr)

            self.x = np.array(patches_src)
            self.y = np.array(patches_dst)

        elif self.data_mode == self.DataMode_VAL():
            self.x = np.array(img_buff_src)
            self.y = np.array(img_buff_dst)

        else:  # case: product
            self.x = np.array(img_buff_src)
            self.y = np.array([])
        
        print('Loading completed.')

    def reload(self, path):
        self.path = path
        self.load()

    def preview(self, max_frames: int = 100, col: int = 5, scale=2):
        if self.x.shape[0] < max_frames:
            max_frames = self.x.shape[0]

        if (self.data_mode == self.DataMode_TRAIN()) or (self.data_mode == self.DataMode_VAL()):
            if self.channel_mode == self.CH_SINGLE():
                imgs = np.concatenate([self.x[:max_frames, :, :, :], self.y[:max_frames, :, :, :]], axis=2)
            else:
                imgs = np.concatenate([self.x[:max_frames, :, :, :3], self.x[:max_frames, :, :, 3:], self.y[:max_frames, :, :, :]], axis=2)
        else:
            if self.channel_mode == self.CH_SINGLE():
                imgs = self.x[:max_frames, :, :, :]
            else:
                imgs = np.concatenate([self.x[:max_frames, :, :, :3], self.x[:max_frames, :, :, 3:]], axis=2)

        if (self.x.size == self.y_pred.size) or (self.x.size/2 == self.y_pred.size):
            imgs = np.concatenate([imgs[:max_frames, :, :, :], self.y_pred[:max_frames, :, :, :]], axis=2)

        row = math.floor(max_frames/col)
        aspect = imgs.shape[2]/imgs.shape[1]

        fig = plt.figure(figsize=(col*aspect*scale, row*scale))

        imgs = imgs.copy()[:, :, :, ::-1]
        idx = 0
        while idx < row * col:
            plt.subplot(row, col, idx+1)
            plt.imshow(imgs[idx, :, :, :])
            plt.axis('off')
            idx += 1

    def gen_patches(self, img: np.ndarray) -> list:
        init_dim = img.ndim
        if init_dim < 3:
            img = img[:, :, np.newaxis]

        list_region = self.__get_mat_list(img.shape[0:2], self.patch_size, self.stride)

        ret = []
        for idx in range(list_region.shape[2]):
            a = list_region[:, :, idx]
            tmp_patch = img[a[0, 0]:a[0, 1], a[1, 0]:a[1, 1], :]
            ret.append(tmp_patch)

        return ret

    def val_train_split(self, ratio: float = 1/5):
        if self.data_mode == self.DataMode_TRAIN:
            ret_train = self
            ret_val = self
            n = np.round(self.x.shape[0]*ratio)
            x, y = self.__shuffle(self.x, self.y, seed=self.seed)
            ret_train.x = x[:n, :, :, :]
            ret_train.y = y[:n, :, :, :] 

            ret_val.x = x[n:, :, :, :]
            ret_val.y = y[n:, :, :, :]
            ret_val.data_mode = self.DataMode_VAL
            return ret_train, ret_val

        else:
            print('data mode is not ''train'', None will be returned')
            return None

    @staticmethod
    def __augment(data0, data1, fctr=1):
        if fctr < 2:
            return data0, data1

        ret0 = []
        ret1 = []

        print('augmenting data.....')
        for idx in tqdm(range(data0.__len__())):
            tmp0 = data0[idx].copy()
            tmp1 = data1[idx].copy()

            for _ in range(fctr):
                rot = random.randint(0, 3)
                tmp0 = np.rot90(tmp0.copy(), rot)
                tmp1 = np.rot90(tmp1.copy(), rot)

                flip = random.randint(0, 3)
                if flip == 1:
                    tmp0 = np.flipud(tmp0)
                    tmp1 = np.flipud(tmp1)
                elif flip == 2:
                    tmp0 = np.fliplr(tmp0)
                    tmp1 = np.fliplr(tmp1)
                elif flip == 3:
                    tmp0 = np.flipud(tmp0)
                    tmp1 = np.flipud(tmp1)
                    tmp0 = np.fliplr(tmp0)
                    tmp1 = np.fliplr(tmp1)

                ret0.append(tmp0)
                ret1.append(tmp1)

        return ret0, ret1

    @staticmethod
    def __dwnconvert(data: list, mode=cv2.INTER_LINEAR, fctr=2):
        print('')
        print('down converting.....')
        ret = []
        for i, img in enumerate(tqdm(data)):
            sz = img.shape
            img = cv2.resize(img, (int(sz[0]/fctr), int(sz[1]/fctr)), interpolation=cv2.INTER_NEAREST)
            img = cv2.resize(img, (int(sz[0]), int(sz[1])), interpolation=mode)
            data[i] = img
            # ret.append(img)

        return data

    @staticmethod
    def __shuffle(data0, data1, seed=None):
        print('')
        print('shuffle data')
        patches = list(zip(data0, data1))
        random.seed(seed)
        random.shuffle(patches)
        data0, data1 = zip(*patches)
        return data0, data1

    @staticmethod
    def __dropout(data0, data1, threshould: float = 0.99):
        print('')
        print('filtering data.....')
        ret0 = []
        ret1 = []

        for idx in tqdm(range(data0.__len__())):
            tmp0 = data0[idx]
            tmp1 = data1[idx]

            if tmp1.mean() < threshould:
                ret0.append(tmp0)
                ret1.append(tmp1)

        return ret0, ret1

    @staticmethod
    def __arrange_arg(a) -> np.ndarray:
        ret = None
        if (type(a) == int) or (type(a) == float):
            ret = np.array([a, a]).astype(dtype=np.uint16)

        elif (type(a) == list) or (type(a) == tuple):
            if a.__len__() == 1:
                ret = np.array([a, a]).astype(dtype=np.uint16)
            elif a.__len__() == 2:
                ret = np.array(a).astype(dtype=np.uint16)

        elif type(a) == np.ndarray:
            if a.size() == 1:
                ret = np.array([a, a]).astype(dtype=np.uint16)
            elif a.size() == 2:
                ret = a.astype(dtype=np.float64)

        return ret.copy()

    def __get_mat_list(self, img_size, patch_size, stride=10) -> np.ndarray:
        patch_size = self.__arrange_arg(patch_size).astype(np.uint16)
        stride = self.__arrange_arg(stride).astype(np.uint16)

        dim0 = list(range(0, img_size[0]-patch_size[0], stride[0]))
        dim1 = list(range(0, img_size[1]-patch_size[1], stride[1]))

        dim0 = np.array(list(product(dim0, dim1)))
        dim0 = dim0[:, :, np.newaxis].astype(dtype=np.uint16)
        dim1 = dim0.copy()
        dim1[:, 0, :] = dim1[:, 0, :] + patch_size[0]
        dim1[:, 1, :] = dim1[:, 1, :] + patch_size[1]

        ret = np.concatenate((dim0, dim1), axis=2)

        ret = np.transpose(ret, [1, 2, 0])
        return ret

    def __load_img(self, path: str) -> np.ndarray:
        ret = cv2.imread(path, cv2.IMREAD_COLOR).astype(dtype=np.float64)
        self.original_size = ret.shape[:2]
        if all(self.resize):
            ret = cv2.resize(ret, (int(self.resize[0]), int(self.resize[1])), interpolation=cv2.INTER_LINEAR)
        ret = ret / ret.max()
        return ret

    def __resize(self, img: np.ndarray):
        img = cv2.resize(img, (int(self.original_size[0]), int(self.original_size[1])), interpolation=cv2.INTER_LINEAR)
        return img

    @staticmethod
    def __list_path_multi_ch(dir_parent: str, ext: str = 'png'):
        dir_src0 = os.path.join(dir_parent, 'src', 'ch0', '*.' + ext)
        dir_src1 = os.path.join(dir_parent, 'src', 'ch1', '*.' + ext)
        dir_dst = os.path.join(dir_parent, 'dst', '*.' + ext)

        return sorted(glob(dir_src0)), sorted(glob(dir_src1)), sorted(glob(dir_dst))

    @staticmethod
    def __list_path_single_ch(dir_parent: str, ext: str = 'png'):
        dir_src = os.path.join(dir_parent, 'src', '*.' + ext)
        dir_dst = os.path.join(dir_parent, 'dst', '*.' + ext)

        return sorted(glob(dir_src)), sorted(glob(dir_dst))

    @staticmethod
    def __list_path_multi_ch_nodst(dir_parent: str, ext: str = 'png'):
        dir_src0 = os.path.join(dir_parent, 'ch0', '*.' + ext)
        dir_src1 = os.path.join(dir_parent, 'ch1', '*.' + ext)

        return sorted(glob(dir_src0)), sorted(glob(dir_src1))

    @staticmethod
    def __list_path_single_ch_nodst(dir_parent: str, ext: str = 'png'):
        dir_src = os.path.join(dir_parent, '*.' + ext)

        return sorted(glob(dir_src))

    @staticmethod
    def DataMode_VAL() -> str:
        return 'val'

    @staticmethod
    def DataMode_TRAIN() -> str:
        return 'train'

    @staticmethod
    def DataMode_PRODUCT() -> str:
        return 'product'

    @staticmethod
    def CH_SINGLE() -> str:
        return 'single'
    
    @staticmethod
    def CH_MULTI() -> str:
        return 'multi'

