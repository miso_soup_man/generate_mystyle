import w200_ModelTemplate_v1 as mt
from tensorflow.keras.layers import \
    Conv3D, BatchNormalization, Activation, Input, UpSampling2D, \
    Subtract, Lambda, Conv2D, AveragePooling3D, AveragePooling2D, \
    MaxPooling2D, concatenate, Conv2DTranspose
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam
import importlib

importlib.reload(mt)

time = '2020-0404-1541'
print('current U-net version : '+time)


class Unet(mt.ModelTemplate):
    def __init__(self,
                 epochs: int = 300,
                 steps_per_epoch: int = 200,
                 batch_size: int = 128,
                 save_distance: int = 10,
                 weightsave_path: str = './models/flesh_models/'
                 ):

        super().__init__(
                        epochs=epochs,
                        steps_per_epoch=steps_per_epoch,
                        batch_size=batch_size,
                        save_distance=save_distance,
                        weightsave_path=weightsave_path
                        )
        self.modeltype = 'Unet'
        self.init_model()

    def init_model(self):
        def block(input_layer, chs, repeat=2):
            x = input_layer
            for i in range(repeat):
                x = Conv2D(chs, kernel_size=(3, 3), padding='same', strides=(1, 1),
                           use_bias=False, kernel_initializer='Orthogonal')(x)
                x = BatchNormalization()(x)
                x = Activation('relu')(x)
            return x

        model_input = Input((None, None, 3))

        x1 = block(model_input, 64)

        x2 = MaxPooling2D(pool_size=(2, 2))(x1)
        x2 = block(x2, 128)

        x3 = MaxPooling2D(pool_size=(2, 2))(x2)
        x3 = block(x3, 256)

        x4 = MaxPooling2D(pool_size=(2, 2))(x3)
        x4 = block(x4, 512)

        x5 = MaxPooling2D(pool_size=(2, 2))(x4)
        x5 = block(x5, 512, repeat=1)
        x5 = block(x5, 1024, repeat=2)

        x = Conv2DTranspose(512, (2, 2), strides=(2, 2), padding='same')(x5)
        x = concatenate([x, x4], axis=-1)
        x = block(x, 512, repeat=1)

        x = Conv2DTranspose(256, (2, 2), strides=(2, 2), padding='same')(x)
        x = concatenate([x, x3], axis=-1)
        x = block(x, 256, repeat=1)

        x = Conv2DTranspose(128, (2, 2), strides=(2, 2), padding='same')(x)
        x = concatenate([x, x2], axis=-1)
        x = block(x, 128, repeat=1)

        x = Conv2DTranspose(64, (2, 2), strides=(2, 2), padding='same')(x)
        x = concatenate([x, x1], axis=-1)
        x = block(x, 64, repeat=1)

        x = Conv2D(3, kernel_size=(3, 3), strides=(1, 1),
                   kernel_initializer='Orthogonal', padding='same')(x)
        x = Activation('sigmoid')(x)

        # input_0 = model_input
        # x = Subtract()([input_0, x])

        self.model = Model(model_input, x)

