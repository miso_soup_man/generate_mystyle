import w200_ModelTemplate_v1 as mt
from tensorflow.keras.layers import \
    Conv3D, BatchNormalization, Activation, Input, UpSampling2D, \
    Subtract, Lambda, Conv2D, AveragePooling3D, AveragePooling2D, \
    MaxPooling2D, concatenate, Conv2DTranspose, subtract
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam
import importlib
import w301_StyleLoss as stls
import w300_LossFunctions_v1 as ls

importlib.reload(stls)
importlib.reload(mt)

time = '2020-0404-1541'
print('current Res-Net version : '+time)


class ResNet(mt.ModelTemplate):
    def __init__(self,
                 epochs: int = 300,
                 steps_per_epoch: int = 200,
                 batch_size: int = 128,
                 save_distance: int = 10,
                 weightsave_path: str = './models/',
                 input_ch: int = 3,
                 loss_weight=[1., 1., 10000.],
                 kernel_size=3,
                 ):

        super().__init__(
                        epochs=epochs,
                        steps_per_epoch=steps_per_epoch,
                        batch_size=batch_size,
                        save_distance=save_distance,
                        weightsave_path=weightsave_path,
                        input_ch=input_ch,
                        loss_weight=loss_weight,
                        kernel_size=kernel_size,
        )
        self.modeltype = 'ResNet_v1'
        self.counta = 0
        self.init_model()

    def init_model(self):
        kernel_size = self.kernel_size

        def block(input_layer, chs, reps=2, count=0):
            x = input_layer
            for i in range(reps):
                x = Conv2D(chs, kernel_size=(kernel_size, kernel_size), padding='same', strides=(1, 1),
                           use_bias=False, kernel_initializer='Orthogonal',
                           name='conv_{}_{}'.format(count, i))(x)
                x = BatchNormalization(name='BN_{}_{}'.format(count, i))(x)
                x = Activation('relu', name='relu_{}_{}'.format(count, i))(x)
            return x

        model_input = Input((None, None, self.input_ch), name='input_{}'.format(self.counta))

        self.counta += 1
        x1 = block(model_input, 64)

        self.counta += 1
        x2 = block(x1, 128, count=self.counta)

        self.counta += 1
        x3 = block(x2, 256, count=self.counta)

        self.counta += 1
        x4 = block(x3, 256, count=self.counta)

        self.counta += 1
        x = block(x4, 256, reps=1, count=self.counta)

        self.counta += 1
        x = block(x, 256, reps=2, count=self.counta)

        self.counta += 1
        x = block(x, 256, reps=1, count=self.counta)
        x = subtract([x, x4], name='subtract_{}'.format(self.counta))

        self.counta += 1
        x = block(x, 256, reps=1, count=self.counta)
        x = subtract([x, x3], name='subtract_{}'.format(self.counta))

        self.counta += 1
        x = block(x, 128, reps=1, count=self.counta)
        x = subtract([x, x2], name='subtract_{}'.format(self.counta))

        self.counta += 1
        x = block(x, 64, reps=1, count=self.counta)
        x = subtract([x, x1], name='subtract_{}'.format(self.counta))


        self.counta += 1
        x = Conv2D(3, kernel_size=(self.kernel_size, self.kernel_size), strides=(1, 1),
                   kernel_initializer='Orthogonal', padding='same',
                   name='conv_{}'.format(self.counta))(x)
        if self.input_ch == 3:
            x = subtract([model_input, x], name='subtract_{}'.format(self.counta))

        x = Activation('sigmoid', name='output')(x)

        # input_0 = model_input
        # x = Subtract()([input_0, x])

        self.model = Model(model_input, x)

    @staticmethod
    def loss_obj(weight=[1., 1., 1.]):
        def ret_function(y_true, y_pred):
            return weight[0] / 3 + stls.style_loss(y_true, y_pred) \
                   + weight[1] / 3 + stls.content_loss(y_true, y_pred) \
                   + weight[2] / 3 * ls.mse(y_true, y_pred)
        return ret_function

