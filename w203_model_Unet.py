import w200_ModelTemplate_v1 as mt
from tensorflow.keras.layers import \
    Conv3D, BatchNormalization, Activation, Input, UpSampling2D, \
    Subtract, Lambda, Conv2D, AveragePooling3D, AveragePooling2D, \
    MaxPooling2D, concatenate, Conv2DTranspose
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam
import importlib
import w301_StyleLoss as stls
import w300_LossFunctions_v1 as ls

importlib.reload(mt)
importlib.reload(stls)

time = '2020-0412-1208'
print('current Unet_MultiCh version : '+time)


class Unet(mt.ModelTemplate):
    def __init__(self,
                 epochs: int = 300,
                 steps_per_epoch: int = 200,
                 batch_size: int = 128,
                 save_distance: int = 10,
                 weightsave_path: str = './models/flesh_models/',
                 input_ch: int = 3,
                 loss_weight=[1., 1., 10000.]
                 ):

        super().__init__(
                        epochs=epochs,
                        steps_per_epoch=steps_per_epoch,
                        batch_size=batch_size,
                        save_distance=save_distance,
                        weightsave_path=weightsave_path,
                        input_ch=input_ch,
                        loss_weight=loss_weight,
                        )

        self.modeltype = 'Unet_MultiCh'
        self.counta = 0
        self.init_model()

    def init_model(self):
        def block(input_layer, chs, reps=2, count=0):
            x = input_layer
            for i in range(reps):
                x = Conv2D(chs, kernel_size=(3, 3), padding='same', strides=(1, 1),
                           use_bias=False, kernel_initializer='Orthogonal',
                           name='conv_{}_{}'.format(count, i))(x)
                x = BatchNormalization(name='BN_{}_{}'.format(count, i))(x)
                x = Activation('relu', name='relu_{}_{}'.format(count, i))(x)
            return x

        def bridge_layer(layer_1, layer_2, count: int = 0, mode='cat'):
            if mode == 'cat':
                x = concatenate([layer_1, layer_2], axis=-1, name='concat_{}'.format(count))
            else:
                x = Subtract([layer_1, layer_2], axis=-1, name='subtract_{}'.format(count))
            return x

        model_input = Input((None, None, self.input_ch), name='input_{}'.format(self.counta))

        self.counta += 1
        x1 = block(model_input, 64, count=self.counta)

        self.counta += 1
        x2 = MaxPooling2D(pool_size=(2, 2), name='maxpool_{}'.format(self.counta))(x1)
        x2 = block(x2, 128, count=self.counta)

        self.counta += 1
        x3 = MaxPooling2D(pool_size=(2, 2), name='maxpool_{}'.format(self.counta))(x2)
        x3 = block(x3, 256, count=self.counta)

        self.counta += 1
        x4 = MaxPooling2D(pool_size=(2, 2), name='maxpool_{}'.format(self.counta))(x3)
        x4 = block(x4, 512, count=self.counta)

        self.counta += 1
        x5 = MaxPooling2D(pool_size=(2, 2), name='maxpool_{}'.format(self.counta))(x4)
        x5 = block(x5, 512, reps=1, count=self.counta)
        self.counta += 1
        x5 = block(x5, 1024, reps=2, count=self.counta)

        self.counta += 1
        x = Conv2DTranspose(512, (2, 2), strides=(2, 2), padding='same', name='deconv_{}'.format(self.counta))(x5)
        x = concatenate([x, x4], axis=-1, name='concat_{}'.format(self.counta))
        x = block(x, 512, reps=1, count=self.counta)

        self.counta += 1
        x = Conv2DTranspose(256, (2, 2), strides=(2, 2), padding='same', name='deconv_{}'.format(self.counta))(x)
        x = concatenate([x, x3], axis=-1, name='concat_{}'.format(self.counta))
        x = block(x, 256, reps=1, count=self.counta)

        self.counta += 1
        x = Conv2DTranspose(128, (2, 2), strides=(2, 2), padding='same', name='deconv_{}'.format(self.counta))(x)
        x = concatenate([x, x2], axis=-1, name='concat_{}'.format(self.counta))
        x = block(x, 128, reps=1, count=self.counta)

        self.counta += 1
        x = Conv2DTranspose(64, (2, 2), strides=(2, 2), padding='same', name='deconv_{}'.format(self.counta))(x)
        x = concatenate([x, x1], axis=-1, name='concat_{}'.format(self.counta))
        x = block(x, 64, reps=1, count=self.counta)

        self.counta += 1
        x = Conv2D(3, kernel_size=(3, 3), strides=(1, 1),
                   kernel_initializer='Orthogonal', padding='same', name='conv_{}'.format(self.counta))(x)
        x = Activation('sigmoid', name='output')(x)

        # input_0 = model_input
        # x = Subtract()([input_0, x])

        self.model = Model(model_input, x)

    @staticmethod
    def loss_obj(weight=[1., 1., 1.]):
        def ret_function(y_true, y_pred):
            return weight[0] / 3 + stls.style_loss(y_true, y_pred) \
                   + weight[1] / 3 + stls.content_loss(y_true, y_pred) \
                   + weight[2] / 3 * ls.mse(y_true, y_pred)
        return ret_function


