from tensorflow.keras import losses as ls
from tensorflow.keras import backend as K
from tensorflow.keras.applications import vgg19

def mse(y_true, y_pred):
    return K.sum(K.square(y_pred - y_true)) / K.prod(K.cast(K.shape(y_true), 'float32'))
