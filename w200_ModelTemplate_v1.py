from tensorflow.keras.layers import \
    Conv3D, BatchNormalization, Activation, Input, Concatenate, \
    MaxPool3D, UpSampling2D, Subtract, Lambda, Conv2D, AveragePooling3D, \
    AveragePooling2D, MaxPool2D
from tensorflow.keras.models import Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras import backend as K
from tensorflow.keras import callbacks as cb
from tensorflow.keras import losses as ls
from tensorflow.keras.utils import plot_model
from skimage.measure import compare_ssim, compare_psnr
from os import path
import os
import numpy as np
import cv2
from datetime import datetime as dt
from tqdm import tqdm
import importlib
import w100_DataHandler_v1 as dh
import w300_LossFunctions_v1 as myls
import math

importlib.reload(dh)
importlib.reload(myls)

time = '2020-0412-1206'
print('current model template version : '+time)


class ModelTemplate:
    def __init__(self,
                 epochs: int = 300,
                 steps_per_epoch: int = 20,
                 batch_size: int = 128,
                 save_distance: int = 10,
                 weightsave_path: str = './models/flesh_models/',
                 input_ch: int = 3,
                 loss_weight=[1., 1., 10000.],
                 kernel_size=3
                 ):
        self.description = time
        self.epochs = epochs
        self.callbacks = []
        self.steps_per_epoch = steps_per_epoch
        self.batch_size = batch_size
        self.counta = 0
        self.input_ch = input_ch
        self.img_bit_depth = self.BIT8()
        self.weightsave_path = weightsave_path
        self.save_distance = save_distance
        self.modeltype = 'empty'
        self.optimizer = Adam(beta_1=0.9, beta_2=0.999, amsgrad=False)
        self.verbose = 1
        self.loss_weight = loss_weight
        self.kernel_size = kernel_size
        # static properties are defined by here

        self.model = Model()
        self.init_model()

    def reset(self):
        self.__init__()

    def fit(self, dataset: dh.DataSet, reload_data_per_step: bool = True):
        self.init_callbacks()
        lst = range(0, self.epochs + self.steps_per_epoch, self.steps_per_epoch)
        for i, _ in enumerate(lst):
            print('Repeat {}/ {}'.format(i+1, len(lst)))
            self.model.fit(
                *self.organize_data(dataset), batch_size=self.batch_size,
                verbose=self.verbose, epochs=self.steps_per_epoch, callbacks=self.callbacks
            )
            if reload_data_per_step:
                dataset.load()

    def init_model(self):
        raise Exception('Called abstract method: init_model')

    def compile(self):
        self.model.compile(optimizer=self.optimizer, loss=self.loss_obj(weight=self.loss_weight))

    def load_weights(self, path: str):
        self.model.load_weights(path)

    def predict(self, src: dh.DataSet, save_img: bool = False, save_path: str = ''):
        if save_img:
            save_path = path.join(save_path, timestr(3)+'_predicted/')
            if not(path.exists(save_path)):
                os.mkdir(save_path)

        count = 0
        dst = []
        for idx in tqdm(range(src.x.shape[0])):
            curr_src = src.x[idx, :, :, :].copy()[np.newaxis, :, :, :]
            curr_dst = np.clip(np.squeeze(self.model.predict(x=curr_src)), 0, 1)
            curr_dst = np.clip(curr_dst, 0, 1)
            dst.append(curr_dst)

            if save_img:
                save_path_img = path.join(save_path, 'post_{:05d}.png'.format(count))
                if self.img_bit_depth == self.BIT8():
                    curr_dst = curr_dst*255
                    curr_dst = curr_dst.astype(dtype=np.uint8)
                else:
                    curr_dst = curr_dst*65535
                    curr_dst = curr_dst.astype(dtype=np.uint16)

                cv2.imwrite(save_path_img, curr_dst)

            count += 1

        src.y_pred = np.array(dst)

        return src.y_pred, src

    def validate(self, data: dh.DataSet):
        x, y_true = self.split_data(data)
        score_loss = 0
        y_pred = []
        for idx in range(len(x)):
            score_loss += self.model.evaluate(x[idx], y_true[idx])
            y_pred.append(np.clip(np.squeeze(self.model.predict(x=x[idx])), 0, 1))

        score_loss /= (len(x)*x[0].size)
        data.y_pred = np.array(y_pred)

        iq_0 = 0
        iq_1 = 0

        for idx in range(len(y_pred)):
            iq_0 += self.evaluate_image_quality(y_pred[idx], np.squeeze(y_true[idx]))
            if x[idx].shape == y_true[idx].shape:
                iq_1 += self.evaluate_image_quality(np.squeeze(x[idx]), np.squeeze(y_true[idx]))

        iq_0 /= len(y_pred)
        iq_1 /= len(y_pred)

        print('loss: {:.5f}, measured IQ: {:.5f} (before IQ: {:.5f})'.format(score_loss, iq_0, iq_1))

        return score_loss, iq_1, data

    def summary(self):
        self.model.summary()

    def plot_model(self, path: str = 'model.png'):
        plot_model(self.model, to_file=path)
        plot_model(self.model)

    def init_callbacks(self):
        ret = []
        fpath = path.join(self.weightsave_path, self.modeltype + '_weights.{epoch:0=5}-{loss:.5f}.hdf5')
        tmp = cb.ModelCheckpoint(
            filepath=fpath, verbose=1, save_weights_only=True, period=self.save_distance
        )
        ret.append(tmp)
        self.callbacks = ret

    @staticmethod
    def organize_data(data: dh.DataSet):
        return data.x, data.y

    @staticmethod
    def split_data(data: dh.DataSet):
        return_x = []
        return_y = []
        for idx in range(data.x.shape[0]):
            curr_x = data.x[idx, :, :, :].copy()[np.newaxis, :, :, :]
            curr_y = data.y[idx, :, :, :].copy()[np.newaxis, :, :, :]
            return_x.append(curr_x)
            return_y.append(curr_y)

        return return_x, return_y

    @staticmethod
    def loss_obj(weight=[1., ]):
        def local_mse(y_true, y_pred):
            return weight[0] * myls.mse(y_true, y_pred)
        return local_mse

    @staticmethod
    def evaluate_image_quality(y_pred: np.ndarray, y_true: np.ndarray):
        def calc_psnr(y_pred: np.ndarray, y_true: np.ndarray):
            ret = np.sqrt(np.sum((y_pred - y_true) ** 2)) / y_true.size
            ret = -10 * math.log10(ret)
            return ret
        ret = compare_ssim(y_pred, y_true, multichannel=True)
        return ret

    @staticmethod
    def BIT16():
        return '16'

    @staticmethod
    def BIT8():
        return '8'


def timestr(mode=0):
    tmp = dt.now()
    ret = 'xxxx-xxxx_'

    if mode == 0:
        print('*** help docs ****')
        print('1: 2019-0329')
        print('2: 2019-0329_')
        print('3: 2019-0329-1201')

    elif mode == 1:
        ret = "{0:%Y-%m%d}".format(tmp)

    elif mode == 2:
        ret = "{0:%Y-%m%d_}".format(tmp)

    elif mode == 3:
        ret = "{0:%Y-%m%d-%H%M}".format(tmp)

    return ret
