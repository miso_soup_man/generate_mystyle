import cv2
import numpy as np
from glob import glob
from itertools import product
import os
from tqdm import tqdm
import random

time = '2020-0322-2117'
print('current loader version : '+time)


def load_val_data(path, ext: str = 'png', augment=1):
    path = list_path(path, ext)

    ret_dst = []
    ret_src = []

    for idx_0 in tqdm(range(path.__len__())):
        for idx_1 in range(path[idx_0].__len__()):
            ret_dst.append(load_img(path[idx_0][idx_1][1]))
            ret_src.append(load_img(path[idx_0][idx_1][0]))

    if augment > 1:
        ret_src, ret_dst = __augment(ret_src, ret_dst, augment)

    return ret_src, ret_dst


def load_predict_data(path, ext: str = 'png'):
    ret = []
    list_dir = sorted(glob(os.path.join(path, '*.' + ext)))
    for tmp in list_dir:
        foo = load_img(tmp)
        foo = foo + np.random.normal(0, 0.075, size=foo.shape)
        ret.append(foo)

    return ret


def load_datapatches(path, patch_size=64, stride=10, ext: str = 'png', argument=1):
    print('loading data from ' + path)

    path = list_path(path, ext)

    buff_src = []
    buff_dst = []
    patches_dst = []
    patches_src = []

    for idx_shot in range(path.__len__()):
        for idx_img in tqdm(range(path[idx_shot][0].__len__())):
            curr_path_src = path[idx_shot][0][idx_img]
            curr_path_dst = path[idx_shot][1][idx_img]

            buff_src.append(load_img(curr_path_src))
            buff_dst.append(load_img(curr_path_dst))

    buff_src = np.array(buff_src)
    buff_dst = np.array(buff_dst)
    buff_src = np.transpose(buff_src, [1, 2, 3, 0])
    buff_dst = np.transpose(buff_dst, [1, 2, 3, 0])

    for fr in range(buff_src.shape[3]):
        img_src = buff_src[:, :, :, fr]
        img_dst = buff_dst[:, :, :, fr]

        patches_src = patches_src + gen_patches(img_src, patch_size, stride)
        patches_dst = patches_dst + gen_patches(img_dst, patch_size, stride)

    if argument > 1:
        patches_src, patches_dst = __augment(patches_src, patches_dst, argument)

    patches_src, patches_dst = __dropout(patches_src, patches_dst)
    # patches_src = __dwnconvert(patches_src)

    patches = list(zip(patches_dst, patches_src))
    random.shuffle(patches)
    patches_dst, patches_src = zip(*patches)
    ret0 = np.array(patches_src)
    ret0 = ret0 + np.random.normal(0, 0.075, size=ret0.shape)

    ret1 = np.array(patches_dst)
    # print(ret0.shape)
    # print(ret1.shape)

    return ret0, ret1


def __dwnconvert(data: list, mode=cv2.INTER_NEAREST, fctr=2):
    print('down converting.....')
    ret = []
    for img in tqdm(data):
        sz = img.shape[0:2]
        img_dst = cv2.resize(img, (int(sz[0]/fctr), int(sz[1]/fctr)), interpolation=mode)
        img_dst = cv2.resize(img_dst, (int(sz[0]), int(sz[1])), interpolation=cv2.INTER_LINEAR)
        ret.append(img_dst)

    return ret


def __augment(data0, data1, fctr=1):
    if fctr < 2:
        return data0, data1

    ret0 = []
    ret1 = []

    print('augmenting data.....')
    for idx in tqdm(range(data0.__len__())):
        tmp0 = data0[idx].copy()
        tmp1 = data1[idx].copy()

        for _ in range(fctr):
            rot = random.randint(0, 3)
            tmp0 = np.rot90(tmp0, rot)
            tmp1 = np.rot90(tmp1, rot)

            flip = random.randint(0, 3)
            if flip == 1:
                tmp0 = np.flipud(tmp0)
                tmp1 = np.flipud(tmp1)
            elif flip == 2:
                tmp0 = np.fliplr(tmp0)
                tmp1 = np.fliplr(tmp1)
            elif flip == 3:
                tmp0 = np.flipud(tmp0)
                tmp1 = np.flipud(tmp1)
                tmp0 = np.fliplr(tmp0)
                tmp1 = np.fliplr(tmp1)

            ret0.append(tmp0)
            ret1.append(tmp1)

    return ret0, ret1


def __dropout(data0, data1, threshould:float = 0.99):
    ret0 = []
    ret1 = []

    print('filtering data.....')
    for idx in tqdm(range(data0.__len__())):
        tmp0 = data0[idx].copy()
        tmp1 = data1[idx].copy()

        if tmp1.mean() < threshould:
            ret0.append(tmp0)
            ret1.append(tmp1)

    return ret0, ret1


def list_path(dir_parent: str, ext: str = 'png'):
    dir_shot = sorted(glob(os.path.join(dir_parent, '*')))
    ret = []

    for cd_shot in dir_shot:
        # print(cd_shot)
        cd_shot_src = os.path.join(cd_shot, 'src', '*.' + ext)
        cd_ls_src = sorted(glob(cd_shot_src))

        cd_shot_dst = os.path.join(cd_shot, 'dst', '*.' + ext)
        cd_ls_dst = sorted(glob(cd_shot_dst))

        tmp_ret = [cd_ls_src, cd_ls_dst]
        ret.append(tmp_ret)

    return ret


def load_img(path: str) -> np.ndarray:
    ret = cv2.imread(path, cv2.IMREAD_COLOR).astype(dtype=np.float64)
    ret = ret / ret.max()
    return ret


def gen_patches(img: np.ndarray, patch_size=64, stride=10) -> list:
    ndim__ = img.ndim
    if ndim__ < 3:
        img = img[:, :, np.newaxis]

    list_region = get_mat_list(img.shape[0:2], patch_size, stride)

    ret = []
    for idx in range(list_region.shape[2]):
        a = list_region[:, :, idx]
        tmp_patch = img[a[0, 0]:a[0, 1], a[1, 0]:a[1, 1], :]

        if ndim__ < 3:
            tmp_patch = np.squeeze(tmp_patch, axis=2)

        ret.append(tmp_patch)

    return ret


def get_mat_list(img_size, patch_size, stride=10) -> np.ndarray:
    patch_size = arrange_arg(patch_size).astype(np.uint16)
    stride = arrange_arg(stride).astype(np.uint16)

    dim0 = list(range(0, img_size[0]-patch_size[0], stride[0]))
    dim1 = list(range(0, img_size[1]-patch_size[1], stride[1]))

    dim0 = np.array(list(product(dim0, dim1)))
    dim0 = dim0[:, :, np.newaxis].astype(dtype=np.uint16)
    dim1 = dim0.copy()
    dim1[:, 0, :] = dim1[:, 0, :] + patch_size[0]
    dim1[:, 1, :] = dim1[:, 1, :] + patch_size[1]

    ret = np.concatenate((dim0, dim1), axis=2)

    ret = np.transpose(ret, [1, 2, 0])
    return ret


def arrange_arg(a) -> np.ndarray:
    ret = None
    if (type(a) == int) or (type(a) == float):
        ret = np.array([a, a]).astype(dtype=np.uint16)

    elif (type(a) == list) or (type(a) == tuple):
        if a.__len__() == 1:
            ret = np.array([a, a]).astype(dtype=np.uint16)
        elif a.__len__() == 2:
            ret = np.array(a).astype(dtype=np.uint16)

    elif type(a) == np.ndarray:
        if a.size() == 1:
            ret = np.array([a, a]).astype(dtype=np.uint16)
        elif a.size() == 2:
            ret = a.astype(dtype=np.float64)

    return ret.copy()






