# 統計のやつ

参考URL : https://bellcurve.jp/statistics/course/

## 周辺確率
同時確率分布がわかっているときに部分集合の確率分布を求める
$$
P(A)=\sum _{B}P(A \cap B)\quad or \int P(A \cap B) \quad \cdots\quad(1)
$$

## 条件付き確率
Bが起こった、という条件の下でAが起こる確率を条件付き確率$P(A|B)$と呼ぶ。  
$$P(A|B)=\frac {P(A\cap B)}{P(B)}\quad \cdots\quad(2)$$
$(2)$ 式より、
$$
P(A \cap B)=P(A|B)P(B)\quad \cdots\quad(3)
$$
増やすと、
$$P(A \cap B\cap C) = P(A | B\cap C)P(B\cap C)$$
$$P(B\cap C)=P(B|C)P(C)$$
したがって、
$$P(A \cap B\cap C) = P(A | B\cap C)P(B|C)P(C)$$
これを**確率の連鎖律**もしくは**確率の乗法定理**という

## ベイズの定理
ある結果から原因を推定することを考える。
事象$A$が起きるという条件下で、互いに排反な$k$種類の事象$B_i$が起こるとする。この時の条件付き確率は、
$$P(B_i|A)=\frac {P(A\cap B_i)}{P(A)}=\frac {P(B_i)P(A| B_i)}{P(A)}\quad \cdots\quad(2)$$
ここで、
$$P(A)=\sum _i ^k P(A \cap B_i)$$
乗法定理より、
$$P(A \cap B_i) = P(B_i)P(A | B_i)$$
であるから、
$$P(A)=\sum _i ^k P(A \cap B_i)=\sum _i ^k P(B_i)P(A | B_i)\quad \cdots\quad(3)$$
$(3)$を混合分布と呼び、$B_i$を潜在変数と呼ぶことがある。$(3)$式を$(2)$式へ代入し、
$$P(B_i|A)=\frac{P(B_i)P(A| B_i)}{\sum _i ^kP(B_i)P(A | B_i)}$$

## 事前確率と事後確率
仮に、3つある袋を選んで、玉を取り出す、という動作をした場合、
3つある袋のうちからn番目の袋を選ぶ確率を「事前確率」という
出てきた玉が白だった場合の袋がn番目である確率を「事後確率」という
玉の色を確認する、という事象によって確率が変動するイメージ

# 一般的な確率分布
## ベルヌーイ分布
二項分布！以上！

## マルチヌーイ分布
ベルヌーイが2値しかとれないのに対し、マルチヌーイではk個の異なる状態をとる一つの離散変数における分布。カテゴリカル分布って言えば一発で伝わる。

## ガウス分布
正規分布。
$$
\mathcal{N}(x;\mu, \sigma^2) = \sqrt{\frac{1}{2\pi \sigma}}exp \left(-\frac{1}{2 \sigma^2}(x-\mu)^2\right)
$$
変数を2つ以上に拡張したものは多変数正規分布と呼ばれ、次式で表される
$$
\mathcal{N}(x;\mathbf{\mu}, \mathbf{\Sigma}) = \sqrt{\frac{1}{(2\pi)^n det(\mathbf{\Sigma})}}exp \left(-\frac{1}{2}(x-\mu)^\top\Sigma^{-1}(x-\mu)\right)
$$
ここで$\Sigma$は共分散行列であり、$\mu$は平均値のベクトル。

みた感じn次元のガウス分布って感じ
