import cv2
import numpy as np
from glob import glob
import os
from tqdm import tqdm
from x110_load_DnSR_v4 import load_img
import matplotlib.pyplot as plt
import sys

eps = sys.float_info.epsilon

time = '2020-0316-2005'
print('current reconstructor version : '+time)


def load_predict_data(path, ext: str = 'png', temp_width=1):
    buff = []
    ret = []
    list_dir = sorted(glob(os.path.join(path, '*.' + ext)))
    for tmp in list_dir:
        buff.append(load_img(tmp))

    buff = np.transpose(np.array(buff), [1, 2, 0, 3])
    for fr in range(0, buff.shape[3] - 2*temp_width):
        ret.append(buff[:, :, fr:(1+fr+2*temp_width), :])

    return ret


def load_predict_data_lr(path, ext: str = 'png', temp_width=1):
    buff = []
    ret = []
    list_dir = sorted(glob(os.path.join(path, '*.' + ext)))
    for tmp in list_dir:
        buff.append(load_img(tmp))

    buff = np.transpose(np.array(buff), [1, 2, 0, 3])
    for fr in range(0, buff.shape[3] - 2*temp_width):
        tmp = buff[:, :, fr:(1+fr+2*temp_width), :]
        tmp = __dwnconvert(tmp, fctr=2)
        ret.append(tmp)

    return ret


def __dwnconvert(data: list, mode=cv2.INTER_LINEAR, fctr=2):
    print('down converting.....')
    ret = []
    for img in tqdm(data):
        sz = img.shape[0:2]
        img_dst = []
        for fr in range(img.shape[2]):
            curr_img = np.squeeze(img[:, :, fr, :])
            img_dst.append(cv2.resize(curr_img, (int(sz[0]/fctr), int(sz[1]/fctr)), interpolation=mode))
        img_dst = np.array(img_dst)
        ret.append(np.transpose(img_dst, [1, 2, 0, 3]))

    return ret


def get_alpha(img_sz, overlap):
    img_sz = arrange_arg(img_sz)
    overlap = arrange_arg(overlap)
    if img_sz[0] < overlap[0]/2:
        print('Inputted overlap area exceeds the image size. Half of the image size is used.')
        img_sz[0] = overlap[0] / 2
    if img_sz[1] < overlap[1]/2:
        print('Inputted overlap area exceeds the image size. Half of the image size is used.')
        img_sz[1] = overlap[1] / 2

    ret = []
    for dim in range(2):
        foo = np.linspace(-1.0 * img_sz[dim], 1.0 * img_sz[dim], img_sz[dim])
        foo = (img_sz[dim] - np.abs(foo)) * 0.5
        foo = np.clip(foo/overlap[dim], 0, 1)
        ret.append(foo)

    x0, x1 = np.meshgrid(ret[0], ret[1])
    ret = x0 * x1
    return ret * (1-eps) + eps


def arrange_arg(a) -> np.ndarray:
    ret = None
    if (type(a) == int) or (type(a) == float):
        ret = np.array([a, a]).astype(dtype=np.uint16)

    elif (type(a) == list) or (type(a) == tuple):
        if a.__len__() == 1:
            ret = np.array([a, a]).astype(dtype=np.uint16)
        elif a.__len__() == 2:
            ret = np.array(a).astype(dtype=np.uint16)

    elif type(a) == np.ndarray:
        if a.size() == 1:
            ret = np.array([a, a]).astype(dtype=np.uint16)
        elif a.size() == 2:
            ret = a.astype(dtype=np.float64)

    return ret.copy()


def view(data: np.ndarray, fignum: int = 100):
    data = data.copy()
    plt.clf()
    fig, ax = plt.subplots(num=fignum)
    ax.imshow(np.squeeze(data))
    plt.show()
    plt.pause(0.5)


def get_split_coordinate(img_sz, overlap, patchsize):
    img_sz = arrange_arg(img_sz)
    overlap = arrange_arg(overlap)
    patchsize = arrange_arg(patchsize)

    if img_sz[0] < overlap[0]/2:
        print('Inputted overlap area exceeds the image size. Half of the image size is used.')
        img_sz[0] = overlap[0] / 2
    if img_sz[1] < overlap[1]/2:
        print('Inputted overlap area exceeds the image size. Half of the image size is used.')
        img_sz[1] = overlap[1] / 2

    eff_patsz = patchsize
    out_patsz = patchsize + 2 * overlap

    coord_start = []
    coord_stop = []
    for dim in range(2):
        foo = np.arange(0, img_sz[dim], eff_patsz[dim])
        bar = foo + out_patsz[dim]
        coord_start.append(foo)
        coord_stop.append(bar)

    # meshgrid!!

    coord_start = np.array(coord_start)
    coord_stop = np.array(coord_stop)

    coord = np.array([coord_start, coord_stop])
    coord = np.transpose(coord, [2, 1, 0])  # [start/stop, dim, patch] ->[patch, dim, start/stop]
    return coord


def split_image(img: np.ndarray, overlap, patchsize):
    img = aug_dim(img, 4)
    img_sz = img.shape[0:2]
    overlap = arrange_arg(overlap)
    patchsize = arrange_arg(patchsize)
    coord = get_split_coordinate(img_sz, overlap, patchsize)
    alpha = get_alpha(img_sz, overlap)

    max0 = coord[:, 0, :].max()
    max1 = coord[:, 1, :].max()

    buff = np.zeros((max0, max1, img.shape[2], 4), dtype=np.float64)  # BGRA
    buff[0:img_sz[0]+1, 0:img_sz[1]+1, :, 0:4] = img[:, :, :, :]

    ret = []
    for idx in range(coord.shape[0]):
        cc = coord[idx, :, :]
        cp = buff[coord[idx, 0, 0]:coord[idx, 0, 1], coord[idx, 1, 0]:coord[idx, 1, 1], :, :]
        cp[:, :, :, 3] = alpha
        ret.append([cp, cc])

    return ret, img_sz


def conb_image(patches, img_sz):
    buff = []
    for patch in patches:
        buff.append(patch[1])
    buff = np.array(buff)
    max0 = buff[:, 0, :].max()
    max1 = buff[:, 1, :].max()
    ret = np.zeros((max0, max1, 3), dtype=np.float64)

    for patch in patches:
        alpha = patch[0][:, :, 3]
        img = patch[0][:, :, 0:3]
        cc = patch[1]
        ret[cc[0, 0]:cc[0, 1], cc[1, 0]:cc[1, 1], :] = ret[cc[0, 0]:cc[0, 1], cc[1, 0]:cc[1, 1], :] + img*alpha

    ret = ret[0:img_sz[0], 0:img_sz[1], :]

    return ret


def aug_dim(a: np.ndarray, dim: int = 4) -> np.ndarray:
    while True:
        if a.ndim >= dim:
            return a
        a = np.expand_dims(a, a.ndim)


def redu_dim(a: np.ndarray, dim: int = 4) -> np.ndarray:
    while True:
        if a.ndim <= dim:
            return a
        a = np.squeeze(a, a.ndim-1)


if __name__ == '__main__':
    a = get_alpha(128, 32)
    view(a, 100)
