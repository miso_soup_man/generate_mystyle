import x100_load_v1 as ld
from keras.layers import Conv2D, BatchNormalization, Activation, Input, Concatenate, MaxPool2D, Conv2DTranspose, Add, Masking
from keras.models import Model
from keras.optimizers import Adam
from keras.objectives import mean_squared_error
from keras import backend as K
from keras import callbacks as cb
from keras import losses
from os import path
import os
import numpy as np
import cv2
import matplotlib.pyplot as plt
from datetime import datetime as dt

print('current version : 2020-0306-2212')


class CNN:
    # construction
    def __init__(self, path_train):
        # initialize
        self.path_train = path_train
        self.data_val = []
        self.data_pred = []
        self.epochs = 300
        self.patch_size = 64
        self.stride = 48
        self.augment = 5
        self.callbacks = []
        self.model_path = ''
        self.modeltype = 'unet_v1'
        self.model = Model()
        self.save_distance = 10
        self.ext = 'png'
        self.steps_per_epoch = 200
        self.batch_size = 100

        # construction
        self.gen_train = ld.Generator(self.path_train,
                                      ext=self.ext,
                                      patch_size=self.patch_size,
                                      stride=self.stride,
                                      steps_per_epoch=self.steps_per_epoch,
                                      batch_size=self.batch_size,
                                      epoch_num=self.epochs
                                      )

        self.init_model()
        self.init_callbacks()
        self.optimizer = Adam(beta_1=0.9, beta_2=0.999, amsgrad=False)
        self.model.summary()

    def reset(self):
        self.__init__()

    # # preparation
    def load_weights(self, path):
        self.model.load_weights(path)
        self.model.compile(optimizer=self.optimizer, loss='mse')

    def init_model(self):
        if self.modeltype == 'unet_v1':
            self.model = self.unet_v1()

    def unet_v1(self):
        self.modeltype = 'unet_v1'
        model_input = Input((None, None, 6))

        # encoder
        block1 = self.__model_block(model_input, 32)
        x = MaxPool2D(2)(block1)
        block2 = self.__model_block(x, 64)
        x = MaxPool2D(2)(block2)
        block3 = self.__model_block(x, 128)
        x = MaxPool2D(2)(block3)
        block4 = self.__model_block(x, 256)
        x = MaxPool2D(2)(block4)

        # bottom
        x = MaxPool2D(2)(block4)
        x = self.__model_block(x, 512)

        #  decoder
        x = Conv2DTranspose(256, kernel_size=2, strides=2)(x)
        x = self.__model_block(x, 256)

        x = Conv2DTranspose(128, kernel_size=2, strides=2)(x)
        x = self.__model_block(x, 128)

        x = Conv2DTranspose(64, kernel_size=2, strides=2)(x)
        x = self.__model_block(x, 64)

        x = Conv2DTranspose(32, kernel_size=2, strides=2)(x)
        x = self.__model_block(x, 32)

        x = Conv2D(3, kernel_size=[1, 1])(x)
        x = Activation('sigmoid')(x)

        return Model(model_input, x)

    def __model_block(self, input, chs):
        x = input
        for i in range(2):
            x = Conv2D(chs, 3, padding='same')(x)
            x = BatchNormalization()(x)
            x = Activation('relu')(x)
        return x

    def loss_function(self, y_true, y_pred):
        mses = mean_squared_error(y_true, y_pred)
        return K.sum(mses, axis=(1, 2, 3))

    def compile_train(self):
        self.model.compile(optimizer=self.optimizer, loss='mse')

        self.model.fit_generator(
            generator=self.gen_train, verbose=1, steps_per_epoch=self.steps_per_epoch,
            epochs=self.epochs, callbacks=self.callbacks
        )

    def init_callbacks(self):
        ret = []
        fpath = path.join(self.model_path, self.modeltype + '_weights.{epoch:0=5}-{loss:.5f}.hdf5')
        tmp = cb.ModelCheckpoint(
            filepath=fpath, verbose=1, save_weights_only=False, period=self.save_distance
        )
        ret.append(tmp)
        self.callbacks = ret

    # # evaluation
    def load_val_data(self, path):
        self.data_val = ld.load_val_data(path)

    def evaluate(self, verbose=True):
        x = self.data_val[0]
        img_num = x.__len__()
        ret = 0
        for idx in range(x.__len__()):
            curr_y1 = self.model.predict(x=x[idx])
            curr_y0 = self.data_val[1][idx]
            ret += (curr_y1**2 - curr_y0**2) ** 0.5

        ret = ret/img_num
        if verbose:
            print('mean square error: {:.5f}'.format(ret))

        return ret

    # # prediction
    def load_predict_data(self, tgt_dir: str):
        self.data_pred = ld.load_predict_data(tgt_dir)

    def predict(self, save_img: bool = False, save_path: str = ''):
        ret = []
        for curr_img in self.data_pred:
            pred_img = self.model.predict(x=curr_img)
            ret.append(pred_img)

        if save_img:
            save_path = path.join(save_path, timestr(3)+'_predicted/')
            if not(path.exists(save_path)):
                os.mkdir(save_path)

            for idx in range(ret.__len__()):
                save_path_img = path.join(save_path, '{:05d}.png'.format(idx))
                curr_img = ret[idx]*255
                curr_img = curr_img.astype(dtype=np.uint8)
                cv2.imwrite(save_path_img, curr_img)

        return ret

    def view_train(self, fignum: int = 100, samples: int=100):
        data_in = self.gen_train.data[0]
        data_out = self.gen_train.data[1]

        for idx in range(data_in.shape[0]):
            if (samples < idx) & samples:
                break

            tmp_src0 = np.squeeze(data_in[idx, :, :, 0:3])
            tmp_src1 = np.squeeze(data_in[idx, :, :, 3:6])
            tmp_dst = np.squeeze(data_out[idx, :, :, :])
            tmp_img = np.concatenate((tmp_src0, tmp_src1, tmp_dst), axis=1)
            view(tmp_img, fignum=fignum)


def view(data: np.ndarray, fignum: int = 100):
    data = data.copy()[:, :, ::-1]
    plt.clf()
    fig, ax = plt.subplots(num=fignum)
    ax.imshow(np.squeeze(data))
    plt.show()
    plt.pause(0.5)


def timestr(mode=0):
    tmp = dt.now()
    ret = 'xxxx-xxxx_'

    if mode == 0:
        print('*** help docs ****')
        print('1: 2019-0329')
        print('2: 2019-0329_')
        print('3: 2019-0329-1201')

    elif mode == 1:
        ret = "{0:%Y-%m%d}".format(tmp)

    elif mode == 2:
        ret = "{0:%Y-%m%d_}".format(tmp)

    elif mode == 3:
        ret = "{0:%Y-%m%d-%H%M}".format(tmp)

    return ret


if __name__ == '__main__':
    path_dataset = '/Users/toshikisaito/Google Drive/Gdrive_Document/Python/generate_mystyle/dataset/2020-0306-2150'
    cnn = CNN(path_dataset)
    cnn.compile_train()

    # cnn.load_weights(path.join(cnn.model_path, 'unet_v3_weights.00240-0.00017.hdf5'))
    #
    cnn.load_val_data(path_dataset)
    print('validate loss: {:.5f}'.format(cnn.evaluate()))
    #
    # cnn.model.compile(optimizer=cnn.optimizer, loss=losses.binary_crossentropy)
    test_data = '/Users/toshikisaito/Google Drive/Gdrive_Document/Python/letter_disassemble/data/test_data/'
    cnn.load_test_data(test_data)
    predicted = cnn.predict(save_img=True, save_path=test_data)
