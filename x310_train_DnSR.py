import x210_model_DnSR_v5 as md
from os import path
from keras import losses

cnn = md.CNN()
path_dataset = '/Users/toshikisaito/Google Drive/Gdrive_Document/Python/generate_mystyle/dataset/2020-0311-1636a/'
cnn.augment = 1
cnn.load_train_data(path_dataset)
cnn.compile_train()

cnn.model_path = '/Users/toshikisaito/Google Drive/Gdrive_Document/Python/generate_mystyle/models/2020-0316'
cnn.load_weights(path.join(cnn.model_path, 'resnet_v1_weights.00070-0.00108.hdf5'))
#
# cnn.load_val_data(path_dataset)
# print('validate loss: {:.5f}'.format(cnn.evaluate()))
#
cnn.model.compile(optimizer=cnn.optimizer, loss=losses.binary_crossentropy)
test_data = '/Users/toshikisaito/Google Drive/Gdrive_Document/Python/generate_mystyle/testdata/2020-0312b/src'
cnn.load_predict_data_lr(test_data)
cnn.view_pred(fignum=100)
predicted = cnn.predict(save_img=True, save_path=test_data)
