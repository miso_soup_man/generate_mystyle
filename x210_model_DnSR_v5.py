import x110_load_DnSR_v5 as ld
from keras.layers import \
    Conv3D, BatchNormalization, Activation, Input, Concatenate, \
    MaxPool3D, UpSampling2D, Subtract, Lambda, Conv2D, AveragePooling3D, \
    AveragePooling2D, MaxPool2D
from keras.models import Model
from keras.optimizers import Adam
from keras.objectives import mean_squared_error
from keras import backend as K
from keras import callbacks as cb
from keras import losses
from os import path
import os
import numpy as np
import cv2
import matplotlib.pyplot as plt
from datetime import datetime as dt
from tqdm import tqdm
import importlib

importlib.reload(ld)

time = '2020-0322-2308'
print('current model version : '+time)


class CNN:
    # construction
    def __init__(self):
        # initialize
        self.description = 'default configuration ('+time+')'
        self.data_val = []
        self.data_pred = []
        self.epochs = 300
        self.patch_size = 48
        self.stride = 20
        self.augment = 1
        self.callbacks = []
        self.model_path = ''
        self.modeltype = 'DnCNN'
        self.model = Model()
        self.save_distance = 5
        self.ext = 'png'
        self.steps_per_epoch = 200
        self.batch_size = 128

        # construction
        self.data_train = []
        self.init_model()
        self.init_callbacks()
        self.optimizer = Adam(beta_1=0.9, beta_2=0.999, amsgrad=False)
        # self.model.summary()

    def reset(self):
        self.__init__()

    # # preparation
    def load_weights(self, path):
        self.model.load_weights(path)
        self.model.compile(optimizer=self.optimizer, loss='mse')

    def load_train_data(self, path):
        self.data_train = ld.load_datapatches(
            path, self.patch_size, self.stride, self.ext, self.augment
        )

    def init_model(self):
        if self.modeltype == 'resnet_v1':
            self.model = self.resnet_v1()

        if self.modeltype == 'DnCNN':
            self.model = self.DnCNN()

    def resnet_v1(self):
        def __model_block2D(input_layer, chs):
            x = input_layer
            for i in range(2):
                x = Conv2D(chs, kernel_size=(3, 3), padding='same', strides=(1, 1), use_bias=False)(x)
                x = BatchNormalization()(x)
                x = Activation('relu')(x)
            return x

        self.modeltype = 'resnet_v1'
        model_input = Input((None, None, 3))

        x = __model_block2D(model_input, 8)
        x = __model_block2D(x, 16)
        x = MaxPool2D(2)(x)

        x = __model_block2D(x, 32)
        x = MaxPool2D(2)(x)

        x = __model_block2D(x, 64)
        x = MaxPool2D(2)(x)

        x = __model_block2D(x, 128)
        x = MaxPool2D(2)(x)

        x = __model_block2D(x, 256)

        # x = UpSampling2D(size=(2, 2))(x)
        x = __model_block2D(x, 128)

        x = UpSampling2D(size=(2, 2))(x)
        x = __model_block2D(x, 64)

        x = UpSampling2D(size=(2, 2))(x)
        x = __model_block2D(x, 32)

        x = UpSampling2D(size=(2, 2))(x)
        x = __model_block2D(x, 16)

        x = UpSampling2D(size=(2, 2))(x)
        x = __model_block2D(x, 8)

        x = Conv2D(3, kernel_size=[1, 1])(x)
        x = Activation('sigmoid')(x)

        # input_0 = UpSampling2D(size=(2, 2))(model_input)
        input_0 = model_input
        x = Subtract()([input_0, x])

        return Model(model_input, x)


    def DnCNN(self):
        def __model_block2D(input_layer, chs):
            x = input_layer
            for i in range(2):
                x = Conv2D(chs, kernel_size=(3, 3), padding='same', strides=(1, 1),
                        use_bias=False, kernel_initializer='Orthogonal')(x)
                x = BatchNormalization()(x)
                x = Activation('relu')(x)
            return x

        self.modeltype = 'DnCNN'
        model_input = Input((None, None, 3))

        x = __model_block2D(model_input, 8)
        x = __model_block2D(x, 64)
        x = __model_block2D(x, 64)
        x = __model_block2D(x, 64)
        x = __model_block2D(x, 64)
        x = __model_block2D(x, 64)
        x = __model_block2D(x, 64)
        x = __model_block2D(x, 64)
        x = __model_block2D(x, 64)
        x = __model_block2D(x, 64)
        x = __model_block2D(x, 64)
        x = __model_block2D(x, 64)
        x = __model_block2D(x, 64)
        x = __model_block2D(x, 64)
        x = __model_block2D(x, 64)
        x = __model_block2D(x, 64)
        x = __model_block2D(x, 64)
        x = __model_block2D(x, 64)
        x = __model_block2D(x, 64)

        x = Conv2D(3, kernel_size=(3, 3), strides=(1, 1),
                kernel_initializer='Orthogonal', padding='same')(x)
        x = Activation('sigmoid')(x)

        input_0 = model_input
        x = Subtract()([input_0, x])

        return Model(model_input, x)

    def loss_function(self, y_true, y_pred):
        return K.sum(K.square(y_pred - y_true)) / 2

    def compile_train(self):
        print('model save to '+self.model_path)
        self.init_callbacks()
        self.model.compile(optimizer=self.optimizer, loss='mse')
        self.model.fit(
            x=self.data_train[0], y=self.data_train[1], batch_size=self.batch_size,
            verbose=1, epochs=self.epochs, callbacks=self.callbacks
        )

    def init_callbacks(self):
        ret = []
        fpath = path.join(self.model_path, self.modeltype + '_weights.{epoch:0=5}-{loss:.5f}.hdf5')
        tmp = cb.ModelCheckpoint(
            filepath=fpath, verbose=1, save_weights_only=True, period=self.save_distance
        )
        ret.append(tmp)
        self.callbacks = ret

    # # evaluation
    def load_val_data(self, path):
        self.data_val = ld.load_val_data(path)

    def evaluate(self, verbose=True):
        x = self.data_val[0]
        img_num = x.__len__()
        ret = 0
        for idx in range(x.__len__()):
            curr_y1 = self.model.predict(x=x[idx])
            curr_y0 = self.data_val[1][idx]
            ret += (curr_y1**2 - curr_y0**2) ** 0.5

        ret = ret/img_num
        if verbose:
            print('mean square error: {:.5f}'.format(ret))

        return ret

    # # prediction
    def load_predict_data(self, tgt_dir: str):
        self.data_pred = ld.load_predict_data(tgt_dir)

    def load_predict_data_lr(self, tgt_dir: str):
        self.data_pred = ld.load_predict_data(tgt_dir)

    def predict(self, save_img: bool = False, save_path: str = ''):
        if save_img:
            save_path = path.join(save_path, timestr(3)+'_predicted/')
            if not(path.exists(save_path)):
                os.mkdir(save_path)

        src_list = self.data_pred
        dst_list = []
        count = 0

        for src_img in tqdm(src_list):

            curr_fr = src_img[:, :, :, np.newaxis]
            curr_fr = np.transpose(curr_fr, [3, 0, 1, 2])
            dst_img = np.squeeze(self.model.predict(x=curr_fr))

            dst_img = np.clip(np.squeeze(dst_img), 0, 1)
            dst_list.append(dst_img)

            if save_img:
                save_path_img = path.join(save_path, '{:05d}_denoised.png'.format(count))
                dst_img = np.squeeze(dst_img*65535)
                dst_img = dst_img.astype(dtype=np.uint16)
                cv2.imwrite(save_path_img, dst_img)

                save_path_img = path.join(save_path, '{:05d}_noisy.png'.format(count))
                src_img = np.squeeze(np.clip(np.squeeze(curr_fr), 0, 1)*65535)
                src_img = src_img.astype(dtype=np.uint16)
                cv2.imwrite(save_path_img, src_img)

            count += 1

        return dst_list

    def view_train(self, fignum: int = 100, samples: int=100, switch=0):

        if switch == 3:
            data_in = self.data_train[0]
            data_out = self.data_train[1]
        else:
            data_in = self.data_train[switch]

        for idx in range(data_in.shape[0]):
            if samples == idx:
                break

            if switch == 3:
                tmp_src0 = np.concatenate([data_in[idx, :, :, :], data_out[idx, :, :, :]], axis=1)
            else:
                tmp_src0 = np.squeeze(data_in[idx, :, :, :])

            view(tmp_src0, fignum=fignum)

    def view_pred(self, fignum: int = 100):
        data_in = self.data_pred

        for img in data_in:
            view(img, fignum=fignum)


def view(data: np.ndarray, fignum: int = 100):
    data = data.copy()[:, :, ::-1]
    plt.clf()
    fig, ax = plt.subplots(num=fignum)
    ax.imshow(np.squeeze(data))
    plt.show()
    plt.pause(0.5)


def timestr(mode=0):
    tmp = dt.now()
    ret = 'xxxx-xxxx_'

    if mode == 0:
        print('*** help docs ****')
        print('1: 2019-0329')
        print('2: 2019-0329_')
        print('3: 2019-0329-1201')

    elif mode == 1:
        ret = "{0:%Y-%m%d}".format(tmp)

    elif mode == 2:
        ret = "{0:%Y-%m%d_}".format(tmp)

    elif mode == 3:
        ret = "{0:%Y-%m%d-%H%M}".format(tmp)

    return ret


